<?php

get_header();
?>

<main id="site-content" role="main">

	<?php

	if ( have_posts() ) {

		while ( have_posts() ) { 
			?>
			<p class="text-center px-4"> 
				The following is how the post will appear when viewed by a user. The header and footer will be inherited from Bark.com and a loading spinner will appear while the content loads.
			 </p>

		    <hr>
		    <?php
	    	$img = pods_field('update', $after->ID, 'feature_image', true);
	    	echo wp_get_attachment_image($img['ID'],[970, 335]);
			the_post();

			get_template_part( 'content', get_post_type() );
		}
	}

	?>

</main><!-- #site-content --> 

<?php get_footer(); ?>
