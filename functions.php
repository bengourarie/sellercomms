<?php
/**
 * Register and Enqueue Styles.
 */
function bark_internal_register_styles() {

	$theme_version = wp_get_theme()->get( 'Version' );
	// this should be replaced with a link to the latest CSS
	wp_enqueue_style( 'bark-live-style', 'https://d1w7gvu0kpf6fl.cloudfront.net/css/main_v2-1582116350925.css', array(), $theme_version );
	// this includes the theme's style.css, which just loads in the gordita fonts so things look kinda okay
	wp_enqueue_style( 'style', get_stylesheet_uri() );
}

add_action( 'wp_enqueue_scripts', 'bark_internal_register_styles' );

// These hooks listen for posts being updated, and send our API a payload with the data extracted.
// commenting out the first one, bc pods sends the same data more reliably and less often.
// add_action('post_updated', 'sendUpdateToBark', 10, 3);
add_action('pods_api_post_save_pod_item_update', 'sendUpdateToBark', 10, 3);

function sendUpdateToBark($pieces, $is_new, $id) {
	$barkSendDataOnce = get_option( 'bark_send_after_pods_hook', false );
	if ($barkSendDataOnce !== false) {
		$time = time();
		if ($time < $barkSendDataOnce + 15) {
			return;
		}
	}
	update_option( 'bark_send_after_pods_hook', $time);
	$after = get_post($id);
	$categoryObj = get_the_category($after->ID)[0];
	$categoryArr = [
		'id' => $categoryObj->term_id,
		'name' => $categoryObj->name,
		'primary_color_hex' => pods_field('category', $categoryObj->term_id, 'primary_color_css_hex', true),
		'secondary_color_hex' => pods_field('category', $categoryObj->term_id, 'secondary_color_css_hex', true)
	];
	$targeting = buildTargetingArray($after->ID);
	$img = pods_field('update', $after->ID, 'feature_image', true);
	$content = [
		'id' => $after->ID,
		'title' => $after->post_title,
		'short_content' => pods_field('update', $after->ID, 'short_text', true),
		'feature_image' => wp_get_attachment_url($img['ID']),
		'post_date' => $after->post_date,
		'content' => $after->post_content,
	];
	$body = [
		'post_status' => $after->post_status,
		'post_id' => $after->ID,
		'targeting' => $targeting,
		'content_arr' => $content,
		'category' => $categoryArr
	];
	$headers = [
		'Accept' => 'application/vnd.bark.bwpv1+json'
	];
	$apiurl = BARK_API_URL;
	$path = '/announcements/one/';
	$response = wp_remote_post( 
		$apiurl.$path, 
		array( 
			'headers' => $headers,
			'sslverify' => false,
			'body' => $body,
		) 
	);
	$pod = pods('update', $after->ID);	
	if ( is_wp_error( $response ) ) {
	    $error_message = $response->get_error_message();
	    error_log("Something went wrong: $error_message");
	    $pod->save('last_failed_reason', $error_message);
		$pod->save('last_failed_at', (new DateTime())->format('c'));	
	    return true;
    } else { 
		$resp = $response;
		$body = json_decode($response['body']);	
    	if ($body->status) {
    		$pod->save('last_failed_reason', '');
    		$pod->save('last_failed_at', '');
    	} else {
			$err = is_object($body->error) ? json_encode($body->error->message) : $body->error;
	   		$pod->save('last_failed_reason', $err);
    		$pod->save('last_failed_at', (new DateTime())->format('c'));
    	}
    	return true;
	}
}

function buildTargetingArray($afterId) {
	$targeting = false;
	$targetType = pods_field('update', $afterId, 'audience', true);
	if ($targetType === 'general') {
		$targeting = ['target_type' => 'general'];
	} else if ($targetType === 'experiment') {
		$experimentName = pods_field('update', $afterId, 'experiment_name', true);
		$experimentVariant = pods_field('update', $afterId, 'variant', true);
		$targeting = [
			'target_type' => 'experiment',
			'constraint' => $experimentName ? $experimentName : '',
			'constraint_value' =>  $experimentVariant ? $experimentVariant : '',
		];
	} else if ($targetType === 'country_id') {
		$country = pods_field('update', $afterId, 'country_id', true);
		$targeting = [
			'target_type' => 'country',
			'constraint' => 'country_id',
			'constraint_value' =>  $country ? $country : '',	
		];
	}
	if (!$targeting) {
		// this is an error state.
		$targeting = [];
	}
	return $targeting;
}
