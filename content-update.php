<?php
	$img = pods_field('update', get_the_ID(), 'feature_image', true);
	$categoryObj = get_the_category(get_the_ID())[0];
	$imgurl = wp_get_attachment_url($img['ID']);
  ?>
<div class="container" id="updates_page">
    <div class="d-flex justify-content-start align-items-center mt-3 mb-5">
        <a href="/sellers/updates/" class="text-grey-400 mr-2 text-xs text-decoration-none">Updates</a>
        > 
        <span class="text-dark-blue ml-2 text-xs">
			<?php echo get_the_title();?>

		</span>
    </div>
    <div class="d-flex justify-content-center align-items-center flex-column mb-4">
        <div class="col-12 col-sm-10 col-md-9">
            <h2 class="mb-0 text-center"> <?php echo get_the_title( );?></h2>
        </div>
        <div class="d-flex align-items-center justify-content-center text-xs my-3">
            <span class="text-secondary pr-3">
                <?php echo get_the_date();?>
			</span>
            <span class="rounded-pill px-2 py-1" style="background-color:<?php echo  pods_field('category', $categoryObj->term_id, 'secondary_color_css_hex', true) ;?>; color: <?php echo pods_field('category', $categoryObj->term_id, 'primary_color_css_hex', true);?>;">
                <?php echo $categoryObj->name;?>
            </span>
        </div>
        <div class="col-9 d-flex align-items-center justify-content-center my-2">
            <img class="img-fluid" width="100%" src="<?php echo $imgurl; ?>" alt="Feature Image" style="max-height: 335px;"/>
        </div>
    </div>
    <div class="row justify-content-center mt-3 h-auto">
        <div class="col-xs-12 col-md-10 col-xl-8 h-100 wp-bark-container">
            <?php echo get_the_content();?>
        </div>
    </div>
</div>

